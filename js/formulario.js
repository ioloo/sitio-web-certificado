//	var txtName = document.getElementById('txtName');
//	var txtApellidos = document.getElementById('txtApellidos');
//  var btnEnviar = document.getElementById('btnEnviar');
// ... Utilizo propiedades del DOM asegurandome que no coinciden con otras propiedades de window

var txtName = document.getElementById('txtName');
var txtApellidos = document.getElementById('txtApellidos');
var txtUrl = document.getElementById('txtUrl');
var txtCorreo = document.getElementById('txtCorreo');

var expCorreo = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var expWeb = /^(http|https)\:\/\/[a-z0-9\.-]+\.[a-z]{2,4}/gi;
var expPass = /\w+\W+/;		// falta mayúsculas, puede que [A-Z]+

var validoName = false;
var validoApell = false;
var validoMail = false;
var validoUser = false;
var validoPass = false;

function iniciarFunciones() {

	txtName.onchange = function() {
	    if (txtName.value == "") {
	        //txtUsuario.style.background = "sienna";
   	        divName.className = "has-error form-group has-feedback";
	       	markName.className = "glyphicon glyphicon-remove form-control-feedback show";
	        validoName = false;
	    } else if (txtName.value.length > 20 || txtName.value.length < 4){
	        divName.className = "has-error form-group has-feedback";
	       	markName.className = "glyphicon glyphicon-remove form-control-feedback show";
	        validoName = false;
		}else {
	        //txtUsuario.style.background = "lightcyan";
	        divName.className = "has-success form-group has-feedback";
	       	markName.className = "glyphicon glyphicon-ok form-control-feedback show";
	        validoName = true;
	    }
        compEnvio();
	}

	txtApellidos.onchange = function() {
	    if (txtApellidos.value == "") { 
	        //txtApellidos.style.background = "sienna";
	        validoApell = false;
	        markApell.className = "glyphicon glyphicon-remove form-control-feedback show";
	        divApell.className = "has-error form-group has-feedback";
	    } else {
	        //txtApellidos.style.background = "lightcyan";
	        validoApell = true;
	        divApell.className = "has-success form-group has-feedback";
        	markApell.className = "glyphicon glyphicon-ok form-control-feedback show";
	    }
        compEnvio();
	}

	txtCorreo.onchange = function() {
	    if (txtCorreo.value == "") {
	        //txtCorreo.style.background = "sienna";
   	        markCorreo.className = "glyphicon glyphicon-remove form-control-feedback show";
	        divCorreo.className = "has-error form-group has-feedback";
	    } else if ( !expCorreo.test(txtCorreo.value) ) {
	        alert("Error: La dirección de correo " + txtCorreo.value + " es incorrecta.");
        	divCorreo.className = "has-warning form-group has-feedback";
        	markCorreo.className = "glyphicon glyphicon-warning-sign form-control-feedback show";
	        //txtCorreo.style.background = "red";
	        validoMail = false;
	    } else {
	        //txtCorreo.style.background = "lightcyan";
	        validoMail = true;
        	divCorreo.className = "has-success form-group has-feedback";
	       	markCorreo.className = "glyphicon glyphicon-ok form-control-feedback show";	  
	    }
        compEnvio();
	}

	txtUrl.onchange = function() {
	    if ( !expWeb.test(txtUrl.value) && !(txtUrl.value == "")) {
	        alert("Error: La URL de la web " + txtUrl.value + " es incorrecta.");
	        //txtUrl.style.background = "yellow";
	        divUrl.className = "has-warning form-group has-feedback";
	       	markUrl.className = "glyphicon glyphicon-warning-sign form-control-feedback show";
	    } else {
	        //txtUrl.style.background = "lightcyan";
	        divUrl.className = "has-success form-group has-feedback";
	       	markUrl.className = "glyphicon glyphicon-ok form-control-feedback show";	  
	    }
        compEnvio();
	}
	
}

function alphanumeric(val){
	var regex=/^[0-9A-Za-z]+$/;
	if(regex.test(val)){
		return true;
	} 
	else {
		return false;
	}
}

function compEnvio() {
	if (validoName && validoApell && validoMail) {
		btnEnviar.disabled = false;
	} else {
		btnEnviar.disabled = true;
	}
}